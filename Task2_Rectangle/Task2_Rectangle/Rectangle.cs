using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;

namespace Task2_Rectangle
{
    public class Rectangle
    {
        private Point[] _nodes;

        public Point StartPoint { get; set; }

        public int Height { get; set; }

        public int Width { get; set; }

        public Rectangle(Point botLeft, Point topRight)
        {
            StartPoint = botLeft;
            Height = topRight.Y - botLeft.Y;
            Width = topRight.X - botLeft.X;

            _nodes = new Point[] 
            {
                botLeft,
                topRight,
                new Point(botLeft.X, topRight.Y),
                new Point(topRight.X, botLeft.Y)
            };
        }

        public Rectangle(Point botLeft, int height, int width)
        {
            StartPoint = botLeft;
            Height = height;
            Width = width;

            _nodes = new Point[]
            {
                botLeft,
                new Point(botLeft.X + height, botLeft.Y + width),
                new Point(botLeft.X + height, botLeft.Y),  
                new Point(botLeft.X, botLeft.Y + width)
            };
        }

        public static Rectangle Combine(Rectangle a, Rectangle b)
        {
            var bottomLeft = new Point(0,0);
            var topRight = new Point(0,0);

            var allNodes = a._nodes.Concat(b._nodes);
            var enumerableNodes = allNodes as Point[] ?? allNodes.ToArray();

            bottomLeft.X = enumerableNodes.Min(n => n.X);
            bottomLeft.Y = enumerableNodes.Min(n => n.Y);

            topRight.X = enumerableNodes.Max(n => n.X);
            topRight.Y = enumerableNodes.Max(n => n.Y);

            return new Rectangle(bottomLeft, topRight);
        }

        public Rectangle Move(int moveX, int moveY)
        {
            //for (int i = 0; i < _nodes.Length; i++)
            //{
            //    _nodes[i].X += moveX;
            //    _nodes[i].Y += moveY;
            //}
            var startX = StartPoint.X + moveX;
            var startY = StartPoint.Y + moveY;
            var startPoint = new Point(startX, startY);
            
            return new Rectangle(startPoint, Height, Width);
        }

        public Rectangle Resize(int height, int width)
        {
            Height += height;
            Width += width;

            return new Rectangle(StartPoint, Height, Width);
        }

        public static Rectangle Intersect(Rectangle a, Rectangle b)
        {
            int startX = 0, startY = 0;
            int width = 0, height = 0;

            var minimalAllowed = Math.Max(a.StartPoint.X, b.StartPoint.X);
            var maxAx = a.StartPoint.X + a.Width;
            var maxBx = b.StartPoint.X + b.Width;
            var maximumX = Math.Min(maxAx, maxBx);
            if (maximumX > minimalAllowed)
            {
                var ymin = Math.Max(a.StartPoint.Y, b.StartPoint.Y);
                var ymax1 = a.StartPoint.Y + a.Height;
                var ymax2 = b.StartPoint.Y + b.Height;
                var ymax = Math.Min(ymax1, ymax2);
                if (ymax > ymin)
                {
                    startX = minimalAllowed;
                    startY = ymin;
                    width = maximumX - minimalAllowed;
                    height = ymax - ymin;
                }
            }
            return new Rectangle(new Point(startX, startY), height, width);
        }

        public override string ToString()
        {
            var s = new StringBuilder();
            foreach (var node in _nodes)
                s.Append($"({node.X}, {node.Y}); ");
            return s.ToString();
        }
    }
}
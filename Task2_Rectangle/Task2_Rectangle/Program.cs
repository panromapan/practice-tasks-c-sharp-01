﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Task2_Rectangle
{
    class Program
    {
        static void Main(string[] args)
        {
            var start1 = new Point(1, 1);
            var end1 = new Point(3, 3);

            var start2 = new Point(2, 2);
            var end2 = new Point(5, 5);

            var r1 = new Rectangle(start1, end1);
            var r2 = new Rectangle(start2, end2);
            var r3 = new Rectangle(start1, 5, 10);

            Console.WriteLine($"first one\n  {r1.ToString()}\n");
            Console.WriteLine($"second one\n {r2.ToString()}\n");
            Console.WriteLine($"third one created using another constructor\n {r3.ToString()}\n");
            Console.WriteLine($"result of first and second intersection\n {Rectangle.Intersect(r1, r2)}\n");
            Console.WriteLine($"smallest possible rectangle combined from first and second\n {Rectangle.Combine(r1,r2)}\n");
            Console.WriteLine($"Lets move first one (2,-3)\n {r2.Move(1,1)}\n");
            Console.WriteLine($"Lets change first one`s width and hight by (2,1)\n {r1.Resize(2,1)}\n");

            Console.Read();
        }

        
    }
}

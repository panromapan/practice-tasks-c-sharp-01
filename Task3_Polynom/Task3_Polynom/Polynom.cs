﻿using System;
using System.Text;

namespace Task3_Polynom
{
    internal class Polynom
    {
        private StringBuilder Equation { get; }

        public double Result { get; set; }

        public int[] Coeficient { get; set; }

        public Random random { get; set; }

        public Polynom(int degree)
        {
            Equation = new StringBuilder();
            random = new Random();
            Coeficient = new int[degree];

            for (var i = 0; i < Coeficient.Length; i++)
                Coeficient[i] = random.Next(-10, 10);

            for (var i = degree; i > 0; i--)
            {
                var sign = Coeficient[i - 1] >= 0 ? "+" : "";
                Equation.Append($"{sign}{Coeficient[i - 1]}*x^{i}");
            }
        }

        public Polynom(int[] coefs)
        {
            Equation = new StringBuilder();

            Equation.Append("F(X) =");

            Coeficient = coefs;
        }

        public double Calculate(int x)
        {
            double sum = 0;
            var degree = Coeficient.Length;
            for (int i = degree, j = 0; j <= degree - 1; j++)
            {
                sum += Coeficient[j] * Math.Pow(x, i);
                i--;
            }
            return sum;
        }

        public Polynom Add(Polynom p)
        {
            var length = p.Coeficient.Length;
            var newCoefs = new int[length];
            for (var i = 0; i < length; i++)
                newCoefs[i] = Coeficient[i] + p.Coeficient[i];
            Coeficient = newCoefs;
            return new Polynom(Coeficient);
        }

        public Polynom Substract(Polynom p)
        {
            var length = p.Coeficient.Length;
            var newCoefs = new int[length];
            for (var i = 0; i < length; i++)
                newCoefs[i] = Coeficient[i] - p.Coeficient[i];
            Coeficient = newCoefs;
            return new Polynom(Coeficient);
        }

        public Polynom Multiply(Polynom p)
        {
            var length = p.Coeficient.Length;
            var newCoefs = new int[length];
            for (var i = 0; i < length; i++)
                newCoefs[i] = Coeficient[i] * p.Coeficient[i];
            Coeficient = newCoefs;
            return new Polynom(Coeficient);
        }

        public override string ToString()
        {
            var degree = Coeficient.Length;

            for (int i = degree, j = 0; j <= degree - 1; j++)
            {
                var sign = Coeficient[j] >= 0 ? "+" : "";
                Equation.Append($"{sign}{Coeficient[j]}*x^{i}");
                i--;
            }
            return Equation.ToString();
        }
    }
}
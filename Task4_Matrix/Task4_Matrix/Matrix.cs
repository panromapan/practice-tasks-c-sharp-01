﻿using System;
using System.Collections;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Text;
using static System.Math;

namespace Task4_Matrix
{
    internal class Matrix : IEnumerable
    {
        private readonly int[,] _matrix;

        private readonly int _rows;

        private readonly int _columns;

        public Matrix(int m, int n)
        {
            _matrix = new int[m,n];
            _rows = m;
            _columns = n;
        }
        /// <summary>  
        /// This method populates created matrix with random numbers. 
        /// We need to pass instance of Random class as a parameter, since otherwise 
        /// we will re-seed with the same value many times which results in the same number sequence.
        /// </summary>  
        public void Populate(Random random)
        {
            for (var i = 0; i < _rows; i++)
            for (var j = 0; j < _columns; j++)
                _matrix[i, j] = random.Next(0, 10);
        }

        public static Matrix Add(Matrix a, Matrix b)
        {
            var result = new Matrix(a._columns, a._rows);
            if (a.SameSize(b))
            {
                for (var i = 0; i < a._rows; i++)
                for (var j = 0; j < a._columns; j++)
                    result._matrix[i, j] = a._matrix[i, j] + b._matrix[i, j];
            }
            return result;
        }

        public static Matrix Substract(Matrix a, Matrix b)
        {
            var result = new Matrix(a._columns, a._rows);
            if (a.SameSize(b))
                for (var i = 0; i < a._rows; i++)
                for (var j = 0; j < a._columns; j++)
                    result._matrix[i, j] = a._matrix[i, j] - b._matrix[i, j];
            return result;
        }

        public static Matrix Multiply(Matrix a, Matrix b)
        {
            var result = new Matrix(a._rows, b._columns);

            if (a._rows == b._columns)
                for (var i = 0; i < a._rows; i++)
                for (var j = 0; j < b._columns; j++)
                {
                    for (var k = 0; k < a._columns; k++)
                        result._matrix[i, j] += a._matrix[i, k] * b._matrix[k, j];
                }
            return result;
        }

        public bool SameSize(Matrix matrix)
        {
            if (_columns == matrix._columns && _rows == matrix._rows)
                return true;
            return false;
        }
        /// <summary>
        /// Checks wheter rows are equal to columns for finding Minor
        /// </summary>
        /// <returns>True if rows == columns</returns>
        public bool IsSquare()
        {
            return _rows == _columns;
        }
        /// <summary>
        /// returns new Minor matrix
        /// </summary>
        /// <param name="a">Matrix to operate</param>
        /// <param name="row">Row to delete</param>
        /// <param name="column">Column to delete</param>
        /// <returns>New Matrix object</returns>
        public static Matrix Minor(Matrix a, int row, int column)
        {
            var size = a._rows - 1;
            var matrix = new Matrix(size, size);
            if (!a.IsSquare())
                Console.WriteLine("Not Square");
            else
                for (int i = 0, j=0; i < a._rows; i++)
                {
                    if(i == row)
                        continue;
                    for (int k = 0, u = 0; k < a._columns; k++)
                    {
                        if(k==column)
                            continue;
                        matrix._matrix[j, u] = a._matrix[i, k];
                        u++;
                    }
                    j++;
                }
            return matrix;
        }
        
        /// <summary>
        /// Overriden ToString() to output the matrix object
        /// </summary>
        /// <returns>String representation of matrix</returns>
        public override string ToString()
        {
            var matrix = new StringBuilder();

            for (var i = 0; i < _rows; i++)
            {
                for (var j = 0; j < _columns; j++)
                    matrix.Append($" {_matrix[i, j]} ");
                matrix.Append("\n");
            }
            return matrix.ToString();
        }
        /// <summary>
        /// Enumerator to itterate through Matrix object
        /// </summary>
        /// <returns>IEnumerator for array</returns>
        public IEnumerator GetEnumerator()
        {
            return _matrix.GetEnumerator();
        }
    }
}
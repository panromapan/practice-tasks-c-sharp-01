﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1_Vector
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();

            var v1 = new Vector(5);
            v1.Populate(random);

            var v2 = new Vector(5);
            v2.Populate(random);

            var v3 = new Vector(5);
            var v4 = new Vector(5);

            var v5 = v1.SetInterval(0, 3);

            Console.WriteLine($"First random-item vector = {v1.ToString()}");
            Console.WriteLine($"Second random-item vector = {v2.ToString()}");
            Console.WriteLine($"Empty vector {v3.ToString()}");
            Console.WriteLine($"Interval taken from first vector {v5.ToString()}");
            Console.WriteLine($"Sum of first and second vectors {Vector.Add(v1, v2).ToString()}");
            Console.WriteLine($"Substraction of first and second vectors {Vector.Substract(v1, v2).ToString()}");
            Console.WriteLine($"Multiply first vector by scalar {v1.Multiply(10)}");
            Console.WriteLine($"Are two different reference, but same-value objects are equal? -{v4.Equals(v3)}");


            Console.ReadLine();
        }
    }
}
